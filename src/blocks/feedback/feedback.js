$('.feedback').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  dots: true,
  centerPadding: '143px',
  centerMode: true,
  responsive: [
    {
      breakpoint: 1199,
      settings: {
        centerMode: false,
      }
    }
  ]
});

