$('.slider').slick({
  arrows: false,
  dots: false,
  variableWidth: true,
  infinite: false,
  responsive: [
    {
      breakpoint: 767,
      settings: {
        variableWidth: false
      }
    }
  ]
});