window.sr = ScrollReveal();

sr.reveal('.animated', { 
  origin: 'bottom',
  duration: 500,
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  distance: '50px',
  scale: 1,
  mobile: false,
  reset: false,
  opacity: 0,
  viewFactor: 0.2
});